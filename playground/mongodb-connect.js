// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

// let user = {name: 'saeful', age: 30};
// let {name} = user;
// console.log(name);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (error, database) => {
  const db = database.db('TodoApp')
  if (error) {
     return console.log('unable to connect MongoDB server');
  }
  console.log('connected to MongoDB server');

  // db.collection('Todos').insertOne({
  //   text: 'something to do',
  //   completed: false,
  // }, (error, result) => {
  //   if (error) {
  //     return console.log('unable to insert todo', error);
  //   }
  //   console.log(JSON.stringify(result.ops, undefined, 2));
  // });


  // insert new doc into users (name, age, location)
  // db.collection('Users').insertOne({
  //   name: 'saeful',
  //   age: 30,
  //   location: 'jakarta'
  // }, (error, result) => {
  //   if (error) {
  //     return console.log('unable to insert users', error);
  //   }
  //   console.log(result.ops[0]._id.getTimestamp());
  // });
  
  // console.log(database);

  database.close();
});
