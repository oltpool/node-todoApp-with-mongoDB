// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (error, database) => {
  const db = database.db('TodoApp')
  if (error) {
     return console.log('unable to connect MongoDB server');
  }
  console.log('connected to MongoDB server');

  //////////////////////
  // findOneAndUpdate //
  //////////////////////
  // db.collection('Todos').findOneAndUpdate({
  //   _id: new ObjectID('5b26d972afbcbccca85936b5')
  // }, {
  //   $set: {completed: true}
  // }, {
  //   returnOriginal: false
  // }).then((result) => {
  //   console.log(result);
  // });

  db.collection('Users').findOneAndUpdate({
    _id: new ObjectID('5b26a7c0084b9a14145ceffa')
  }, {
    $set: {name: 'saeful'},
    $inc: {age: 1}
  }, {
    returnOriginal: false
  }).then((result) => {
    console.log(result);
  });


  
  // database.close();
});
