// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (error, database) => {
  const db = database.db('TodoApp')
  if (error) {
     return console.log('unable to connect MongoDB server');
  }
  console.log('connected to MongoDB server');

  /////////////////
  // delete many //
  /////////////////
  // db.collection('Todos').deleteMany({text: 'dinner'}).then((result) => {
  //   console.log(result);
  // });

  ////////////////
  // delete one //
  ////////////////
  // db.collection('Todos').deleteOne({text: 'dinner'}).then((result) => {
  //   console.log(result);
  // });

  /////////////////////////
  // find one and delete //
  /////////////////////////
  // db.collection('Todos').findOneAndDelete({completed: false}).then((result) => {
  //   console.log(result);
  // });

  db.collection('Users').deleteMany({name: 'saeful'});

  db.collection('Users').findOneAndDelete({_id: new ObjectID("5b26a794c28da70b7cd81d86")}).then((results) => {
    console.log(JSON.stringify(results, undefined, 2));
  });

  // database.close();
});
