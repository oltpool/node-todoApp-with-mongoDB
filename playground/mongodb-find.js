// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (error, database) => {
  const db = database.db('TodoApp')
  if (error) {
     return console.log('unable to connect MongoDB server');
  }
  console.log('connected to MongoDB server');

  // db.collection('Todos').find({
  //   _id: new ObjectID('5b26a6668dcf162aac6372fa')
  // }).toArray().then((docs) => {
  //   console.log('Todos');
  //   console.log(JSON.stringify(docs, undefined, 2));
  // }, (error) => {
  //   console.log('unable to fetch todos', error);
  // });

  // db.collection('Todos').find().count().then((count) => {
  //   console.log(`Todos count: ${count}`);
  //   console.log(JSON.stringify(count, undefined, 2));
  // }, (error) => {
  //   console.log('unable to fetch todos', error);
  // });

  db.collection('Users').find({name: 'jane'}).toArray().then((docs) => {
    console.log(JSON.stringify(docs, undefined, 2));
  });

  // database.close();
});
